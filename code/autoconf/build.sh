#!/usr/bin/env bash

aclocal
autoconf
automake --add-missing
./configure
make
