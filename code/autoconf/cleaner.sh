#!/usr/bin/env bash

make clean
rm -rf autom4te.cache src/.deps src/Makefile src/Makefile.in aclocal.m4 \
    config.log config.status depcomp configure COPYING depcomp INSTALL \
    install-sh Makefile missing Makefile.in
