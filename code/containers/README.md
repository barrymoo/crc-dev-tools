### Start Docker (SYSTEM DEPENDENT!!!)
`sudo systemctl start docker`

### Delete Everything

`sudo docker rm $(sudo docker ps -a -q)`
`sudo docker rmi $(sudo docker images -q)`

## Rebuild the Container

`sudo docker build . -t barrymoo/hello:latest`
`sudo docker images`
`sudo docker run barrymoo/hello:latest`
`sudo docker push barrymoo/hello:latest`

## Stop Docker (SYSTEM DEPENDENT!!!)

`sudo systemctl stop docker`

## Run from Singularity

`singularity pull docker://barrymoo/hello:latest`
`singularity run hello-latest.simg`
