#include <array>
#include <iostream>
int main() {
  std::array<int, 3> arr{0, 1, 2};
  for (int i = 0; i < 4; ++i) {
    arr[i] = i;
  }
  return 0;
}
