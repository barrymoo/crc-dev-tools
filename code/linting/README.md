`mkdir build`
`cd build`
`cmake .. -DCMAKE_EXPORT_COMPILE_COMMANDS=ON`
`cd ..`
`ln -s build/compile_commands.json`
`clang-tidy hello.cpp -checks=clang-*,modernize-*`
