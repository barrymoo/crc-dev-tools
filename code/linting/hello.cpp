int main() {
  std::vector<int> v{0, 1, 2};
  for (int i = 0; i < v.size(); ++i) {
    std::cout << "Index: " << v[i] << "\n";
  }
  return 0;
}
