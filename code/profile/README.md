Python
---

`python -m cProfile add.py`

C++
---

- `g++ -o hello hello.cpp`
- `valgrind --tool=callgrind ./hello`
- `ls`
- `kcachegrind callgrind.out.<something>`
