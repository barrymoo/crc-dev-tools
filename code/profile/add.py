#!/usr/bin/env python


def add(a, b):
    return a + b


def add_lists(a, b):
    if len(a) != len(b):
        raise IndexError("Lists aren't same size")
    c = [None] * len(a)
    for idx, (x, y) in enumerate(zip(a, b)):
        c[idx] = add(x, y)
    return c


def add_lists_append(a, b):
    if len(a) != len(b):
        raise IndexError("Lists aren't same size")
    c = []
    for idx, (x, y) in enumerate(zip(a, b)):
        c.append(add(x, y))
    return c


a = range(1000000)
b = range(1000000)
c = add_lists(a, b)
d = add_lists_append(a, b)
