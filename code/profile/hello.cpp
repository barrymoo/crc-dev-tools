#include <vector>
#include <numeric>
#include <stdexcept>
#include <iostream>

std::vector<int> add_vectors(std::vector<int> a, std::vector<int> b) {
  if (a.size() != b.size()) {
    throw std::invalid_argument("Vectors aren't the same size");
  }
  std::vector<int> c(a.size());
  for (size_t i = 0; i < c.size(); ++i) {
    c[i] = a[i] + b[i];
  }
  return c;
}

std::vector<int> add_vectors_emplace_back(std::vector<int> a, std::vector<int> b) {
  if (a.size() != b.size()) {
    throw std::invalid_argument("Vectors aren't the same size");
  }
  std::vector<int> d;
  for (size_t i = 0; i < a.size(); ++i) {
    d.emplace_back(a[i] + b[i]);
  }
  return d;
}

std::vector<int> add_vectors_push_back(std::vector<int> a, std::vector<int> b) {
  if (a.size() != b.size()) {
    throw std::invalid_argument("Vectors aren't the same size");
  }
  std::vector<int> d;
  for (size_t i = 0; i < a.size(); ++i) {
    d.push_back(a[i] + b[i]);
  }
  return d;
}

int main() {
  // Create 2 vectors
  std::vector<int> a(100'000);
  std::vector<int> b(100'000);

  // Fill them, similar to range from Python
  std::iota(a.begin(), a.end(), 0);
  std::iota(b.begin(), b.end(), 0);

  // Add them together
  auto c = add_vectors(a, b);

  // Add them via emplace
  auto d = add_vectors_push_back(a, b);

  // Add them via emplace
  auto e = add_vectors_emplace_back(a, b);

  return 0;
}
