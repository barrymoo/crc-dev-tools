#define CATCH_CONFIG_MAIN
#include "catch.hpp"

int function_from_lib(int a, int b) {
  return a + b;
}

TEST_CASE("Function from lib", "[function_from_lib]"){
  REQUIRE(function_from_lib(1, 2) == 3);
}
