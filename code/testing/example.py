from nose.tools import assert_raises


def function_from_my_library(a, b):
    if type(a) != int:
        raise TypeError("a is not an int! type(a) = {}".format(type(a)))

    if type(b) != int:
        raise TypeError("b is not an int! type(b) = {}".format(type(b)))

    return a + b


def test_add_1_2():
    assert(function_from_my_library(1, 2) == 3)


# def test_add_1_2():
#     assert(function_from_my_library(1, 2) == 5)


def test_add_hello_2():
    assert_raises(TypeError, function_from_my_library, 'hello', 2)


def test_add_2_hello():
    assert_raises(TypeError, function_from_my_library, 2, 'hello')
