#include "gtest/gtest.h"

int function_from_lib(int a, int b) {
  return a + b;
}

TEST(FunctionFromLib, Add_1_2) {
  ASSERT_EQ(3, function_from_lib(1, 2));
}

int main(int argc, char *argv[]) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
