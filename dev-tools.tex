%\PassOptionsToPackae{gray}{xcolor}
\documentclass[hyperref={pdfpagelabels=false},12pt]{beamer}
\setbeamertemplate{frametitle}[default][center]
\mode<presentation>
{
 \usetheme{Warsaw}      % or try Darmstadt, Madrid, Warsaw, ...
 \usecolortheme{default} % or try albatross, beaver, crane, ...
 \usefonttheme{default}  % or try serif, structurebold, ...
 \setbeamertemplate{footline}[frame number]
 \setbeamertemplate{caption}[numbered]
}

\usepackage[utf8]{inputenc}
\usepackage{mathtools}
\usepackage{bm}
\usepackage{helvet}
\usepackage{listings}
\usepackage{gensymb}
\usepackage{array}
\usepackage{times}
\usepackage{xcolor}
\usepackage{default}
\usepackage{ulem}
\usepackage{minted}
\usepackage{hyperref}
\usepackage{booktabs}

% Great Commands
\newcommand{\ig}[2]{\includegraphics[width=#1\linewidth]{#2}}
\newcommand{\mybutton}[2]{\hyperlink{#1}{\beamerbutton{{#2}}}}
\newcommand{\myvbutton}[2]{\vfill\hyperlink{#1}{\beamerbutton{{#2}}}}
\newcommand{\code}[1]{\mintinline{bash}{#1}}
\newcommand{\myurl}[2]{\href{#1}{\color{blue}{#2}}}
\newcommand{\pygment}[3]{\inputminted[bgcolor=lightgray,linenos,fontsize=#1]{#2}{#3}}
\newcommand{\smiley}[0]{\ensuremath{\ddot\smile}}

% Color Scheme
\definecolor{pittblue}{RGB}{28,41,87}
\definecolor{pittgold}{RGB}{205,184,125}
\setbeamercolor{structure}{fg=pittgold}
\setbeamercolor{button}{bg=pittblue}

\title[Development Tools]{{Development Tools}}
\author[Development Tools]{{\textbf{Barry Moore II} (bmooreii), Fangping Mu
(fangping), Shervin Sammak (shs159), Kim Wong (kimwong)}}
\institute[CRC]{Center for Research Computing}
\date{}

\beamertemplatenavigationsymbolsempty

\begin{document}

\begin{frame}[label=started]{Getting Started}
    \begin{itemize}
        \item Download the slides from:
        \url{https://pitt.box.com/v/crc-dev-tools}
        \item I made some assumptions:
        \begin{itemize}
          \item You have access to a Mac or Linux terminal
          \item You know how to run commands in that terminal
          \item You know how to download development tools on that OS
        \end{itemize}
        \item You can follow along with live demos:
    \end{itemize}
    \pygment{\scriptsize}{bash}{source-code/clone.sh}
\end{frame}

\begin{frame}[plain]
\titlepage
\end{frame}

\begin{frame}{Comments before we start}
    \begin{itemize}
      \item Context: This presentation is about other people's software!
      \item This presentation is difficult because:
      \begin{itemize}
        \item I don't know your background
        \item Academic/Scientific code is not unlike the wild west
      \end{itemize}
      \item Examples will be completed in Python or C++
      \item Soak up the ideas, not the contrived examples
      \item Should be equally helpful to your software
    \end{itemize}
\end{frame}

\begin{frame}{Outline}
    \begin{itemize}
        \item ``The Software Life Cycle''
        \begin{itemize}
            \item Writing (Integrated Development Environments [IDEs])
            \item Compilation (Linux and Mac)
            \item Version Control
            \item Code Formatting
            \item Debugging
            \item Profiling
            \item Testing
            \item Continuous Integration/Continuous Deployment
        \end{itemize}
        \item The ``Cloud'' and Computing With It
        \item Containers
        \item General Recommendations
    \end{itemize}
\end{frame}

\begin{frame}{Editors and IDEs}
    \begin{itemize}
      \item Opinion: learn a console editor, start with either \code{vim} or \code{emacs -nw}
      \item Use an IDE when you need more tooling (e.g. built in debugging)
      \item Some options:
      \begin{itemize}
          \item \myurl{https://www.jetbrains.com/student/}{JetBrains Students and Faculty}
          \item \myurl{https://code.visualstudio.com/}{Visual Studio Code}
          \item Text Editors:
          \begin{itemize}
              \item \myurl{https://atom.io/}{Atom} (\textit{I like this so far})
              \item \myurl{https://www.sublimetext.com/}{Sublime Text}
          \end{itemize}
      \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Linux and Mac Compilers}
  \begin{itemize}
    \item C, C++, and Fortran code all needs to be compiled
    \item Standard compilation tools:
    \begin{itemize}
      \item GNU \code{gcc, g++, gfortran} (free)
      \item Clang/LLVM \code{clang, clang++} (free)
      \item Intel \code{icc, icpc, ifort} (dollars)
      \item PGI \code{pgcc, pgc++, pgfortran} (free, dollars, doesn't work on my machine)
      \begin{itemize}
        \item Only compiler with OpenACC support (GCC 7 has some)
      \end{itemize}
      \item NVIDIA \code{nvcc} (part of CUDA package)
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Linux and Mac Compilation}
    \begin{itemize}
      \item \textit{Typically} you use multiple tools to build software
      \item ``In the wild'' you might find the following tool chains:
      \begin{itemize}
        \item \code{./configure && make}
        \item \code{cmake && make} (\textit{my choice at the moment})
        \item \code{cmake && ninja}
        \item \code{bazel}
        \item \code{python}
        \item ``homegrown''
      \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Useful Compilation Flags}
  \begin{itemize}
    \item Compilers are extremely intelligent and should also be used as a tool
    \item Spit out warnings: \code{-Wall, -Wextra}, sometimes you know better
    \item The compiler can optimize your code, start here: \code{-O2}
    \begin{itemize}
      \item \code{-O[0,1,2,3]} are all available
      \item \code{-Og} optimize for debugging (I haven't tested this much)
      \item \code{-Ofast} aggressive optimizations
      \item \code{-Os} optimizations which don't bloat code size
      \item \code{-Oz} reduce code size further than \code{-Os}
    \end{itemize}
    \item Target the architecture you want to run on:
    \begin{itemize}
      \item \code{-march=native} for your own machine
      \item On the cluster, we have lots of architectures:
      \begin{itemize}
        \item GCC 4.8: \code{-march=core-avx2}
        \item GCC 7.3: \code{-march=skylake-avx512}
        \item Intel: \code{-xcommon-avx512}
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{\code{./configure && make}}
    \begin{itemize}
      \item Don't know command line options: \code{./configure --help}
      \item On the cluster you can't install in standard locations: \code{--prefix=<prefix>}
      \item Useful environment variables:
      \begin{itemize}
        \item Compilers: \code{CC, CXX, FC}
        \item Flags: \code{CFLAGS, CXXFLAGS, FCFLAGS, FFLAGS}
      \end{itemize}
      \item Parallel builds
      \begin{itemize}
        \item \code{make -j<N>}, where \code{<N>} is the number of processors
        \item Not guaranteed to succeed! Try \code{make}
      \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{\code{cmake && ...}}
  \footnotesize
  \begin{itemize}
    \item \code{...} can be \code{make} or \code{ninja}
    \item Builds are typically ``out-of-source''
    \item Need help with configuration? \code{ccmake}
    \item Command Line Arguments:
    \begin{itemize}
      \item \code{-DCMAKE_INSTALL_PREFIX=<prefix>}
      \item \code{-DCMAKE_C_COMPILER=<compiler>}
      \item \code{-DCMAKE_C_FLAGS=<flags>}
      \item \code{-DCMAKE_CXX_COMPILER=<compiler>}
      \item \code{-DCMAKE_CXX_FLAGS=<flags>}
      \item \code{-DCMAKE_Fortran_COMPILER=<compiler>}
      \item \code{-DCMAKE_Fortran_FLAGS=<flags>}
    \end{itemize}
    \item \code{CMAKE_BUILD_TYPE}
    \begin{itemize}
      \item Default: empty, i.e. \code{CMAKE_CXX_FLAGS}
      \item \code{CMAKE_CXX_FLAGS_DEBUG}
      \item \code{CMAKE_CXX_FLAGS_RELEASE}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{\code{bazel}}
    \begin{itemize}
      \item New kid on the block
      \item ``out-of-source'' build by default, doesn't appear to have \code{--prefix}
      \item Command line arguments:
      \begin{itemize}
        \item \code{CC, C_COMPILER, CC_FLAGS}, no \code{CXX}
      \end{itemize}
      \item I have only used it for TensorFlow and I didn't like it
      \item Parallel by default, \code{--jobs=<N>}
      \item Actively being developed at Google
    \end{itemize}
\end{frame}

\begin{frame}{Python}
  \begin{itemize}
    \item With \code{pip}: \code{pip install --user <package>}
    \item With \code{setup.py}: \code{python setup.py install --user}
    \item Get your own Python: \myurl{https://conda.io/miniconda.html}{https://conda.io/miniconda.html}
  \end{itemize}
\end{frame}

\begin{frame}{Version Control}
    \begin{itemize}
      \item You found a bug fix, how do you interact with other people's
        software?
      \item Most source code uses a ``version control'' system
      \item 10,000 foot view:
      \begin{itemize}
        \item Keeping track of your changes
        \item Simultaneous editing
        \item Working on new features without affecting main code base
        \item Reviewing potential changes
        \item Backing up your code base
      \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Most Common Tools}
    \begin{itemize}
      \item Bazaar \code{bzr}
      \item Git \code{git}
      \begin{itemize}
        \item I am most familiar with this one
        \item \myurl{https://insights.stackoverflow.com/survey/2017\#work-version-control}{2017 Stack Overflow Survey}: 69.2\%
      \end{itemize}
      \item Mercurial \code{hg}
      \item Subversion \code{svn}
    \end{itemize}
\end{frame}

\begin{frame}{Getting Started}
    \begin{itemize}
      \item Make a \myurl{https://github.com/}{GitHub} Account
      \begin{itemize}
        \item Public by default, private costs dollars
        \item \myurl{https://education.github.com/}{Educational private
        repositories}
      \end{itemize}
      \item Make a \myurl{https://bitbucket.org/}{BitBucket} Account
      \begin{itemize}
        \item Private by default, more collaborators cost dollars
      \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Some Terminology}
    \begin{itemize}
      \item \code{local} - On your machine
      \item \code{remote} - GitHub/BitBucket, other server, etc.
      \item \code{repository} - \code{remote} directory containing code
      \item \code{commit} - difference between current state and previous state
      \item \code{clone} - make local copy of \code{remote} \code{repository}
      \item \code{push} - apply \code{commit} to \code{remote} \code{repository}
      \item \code{pull} - apply \code{commit} from \code{remote} locally
      \item \code{fork} - \code{remote} \code{repository} which is a copy of another
    \end{itemize}
\end{frame}

\begin{frame}{First Repository}
    \begin{itemize}
      \item Build a local directory \code{mkdir <whatever>}
      \item Make your first commit \code{git add --all} \code{git commit -m "Initial Commit"}
      \item Make a remote repository (via web browser)
      \item Push to it (follow instructions from web browser)
      \begin{itemize}
        \item \code{git remote add origin ...}
        \item \code{git push origin master}
      \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{First Pull Request}
    \begin{itemize}
      \item Fork my \myurl{https://github.com/barrymoo/typo}{repository}
      \item Clone your new fork locally \code{git clone ...}
      \item Create a new branch, fix the typo, commit, and push
        \begin{itemize}
          \item \code{git checkout -b fix_typo}
          \item \code{git add README.md}
          \item \code{git commit -m "Fix typo"}
          \item \code{git push origin fix_typo}
        \end{itemize}
      \item Submit PR (from web browser)
    \end{itemize}
\end{frame}

\begin{frame}{Code Formatting and Linting}
  \begin{itemize}
    \item \textit{linting}: statically analyze your source code
      to find potential errors and formatting issues
    \item Example of tools:
    \begin{itemize}
      \item Python: \code{pylint}
      \item C++: \code{clang-tidy}
      \item R: \code{lintr}
    \end{itemize}
    \item Add linting to your editors to spot potential issues
    \item Many IDEs have built-in linting and code formatting
  \end{itemize}
\end{frame}

\begin{frame}{Linting Example}
  \centering
  \Huge Linting Example
\end{frame}

\begin{frame}{Debugging}
  \begin{itemize}
    \item Linting comes back clean, but you still have errors...
    \item Enter debugging, two styles:
    \begin{itemize}
      \item ``print'' - Add print statements until you find the errors
      \item tooling - Use a tool to help you (e.g. IDE)
    \end{itemize}
    \item Terminology:
    \begin{itemize}
      \item \textit{break point} - stop your code at this point
    \end{itemize}
    \item Command line tools:
    \begin{itemize}
      \item Python: \code{pdb}
      \item C++: \code{gdb}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Debugging Example}
  \centering
  \Huge Debugging Example
\end{frame}

\begin{frame}{Profiling and Dynamic Analysis}
  \begin{itemize}
    \item Premature optimization is the root of all evil!
    \item You don't care about performance if you don't profile
    \item Tools:
    \begin{itemize}
      \item Python: \code{cProfile}
      \item C++: \code{valgrind}, \code{gperftools}, \code{vtune}
    \end{itemize}
    \item Valgrind has a lot of functionality:
    \begin{itemize}
      \item Memory leak checking: \code{valgrind --leak-check=yes <program>}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Profiling Example}
  \centering
  \Huge Profiling Example
\end{frame}

\begin{frame}{Testing}
  \begin{itemize}
    \item Testing your code is great way to protect against regressions
    \item Regressions are bugs/issues you have already fixed
    \item Tools:
    \begin{itemize}
      \item Python: \code{nosetests}
      \item C++:
      \begin{itemize}
        \item \myurl{https://github.com/google/googletest/blob/master/googletest/docs/Primer.md}{Google Test}
        \item \myurl{https://github.com/catchorg/Catch2/blob/master/docs/assertions.md\#top}{Catch2}
      \end{itemize}
    \end{itemize}
    \item How do you know someone else's code is working correctly?
  \end{itemize}
\end{frame}

\begin{frame}{CI CD}
  \begin{itemize}
    \item Continuous Integration
    \begin{itemize}
      \item Automate your build, linting, and test steps
      \item For example, connecting Travis CI to GitHub
    \end{itemize}
    \item Continuous Deployment
    \begin{itemize}
      \item Automate your deployment
      \item For \myurl{https://docs.travis-ci.com/user/deployment}{Travis CI}
    \end{itemize}
    \item Other tools:
    \begin{itemize}
      \item \myurl{https://jenkins.io/}{Jenkins}
      \item \myurl{https://about.gitlab.com/}{GitLab}
      \item \myurl{https://circleci.com/}{CircleCI}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Cloud Computing}
  \begin{itemize}
    \item What is the difference between CRC and Cloud Computing?
    \begin{itemize}
      \item CRC resources aren't general purpose
      \item We can't ``burst'' (lots of cores, short time period)
    \end{itemize}
    \item In other words, \textit{pay} to run on someone else's computer
    \item Providers of resources:
    \begin{itemize}
      \item \myurl{https://cloud.google.com/}{Google Compute Platform} (GCP)
      \item \myurl{https://aws.amazon.com/}{Amazon Web Services} (AWS)
      \item \myurl{https://azure.microsoft.com/en-us/}{Microsoft Azure}
      \item \myurl{https://www.alibabacloud.com/}{Alibaba Cloud}
      \item \myurl{https://www.heroku.com/}{Heroku}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Academic Computing on the Cloud}
  \begin{itemize}
    \item It is much easier to get time on our machines!
    \item Educational Grants:
    \begin{itemize}
      \item \myurl{https://cloud.google.com/edu/}{GCP}
      \item \myurl{https://aws.amazon.com/education/awseducate/}{AWS}
    \end{itemize}
    \item Research Grants:
    \begin{itemize}
      \item \myurl{https://research.google.com/research-outreach.html\#/research-outreach/faculty-engagement/faculty-research-awards}{GCP}
      \item \myurl{https://aws.amazon.com/research-credits}{AWS}
      \item \myurl{https://www.microsoft.com/en-us/research/academic-program/microsoft-azure-for-research/}{Azure}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Browse GCP and AWS Consoles}
  \centering
  \Huge Spin up Virtual Machines on GCP and AWS
\end{frame}

\begin{frame}{Containers}
  \begin{itemize}
    \item Think, ``lighter weight virtual machine that runs anywhere''
    \item Tools:
    \begin{itemize}
      \item \myurl{https://www.docker.com/}{Docker}
      \item \myurl{https://github.com/NERSC/shifter}{Shifter} (NERSC)
      \item \myurl{https://github.com/hpc/charliecloud}{Charliecloud} (Los Alamos)
      \item \myurl{https://github.com/singularityware/singularity}{Singularity} (LBL)
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{VM versus Container}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      Virtual Machine \\
      \ig{1.0}{figures/vm}
    \end{column}
    \begin{column}{0.5\textwidth}
      Docker \\
      \ig{1.0}{figures/docker}
    \end{column}
  \end{columns}
  \footnotetext{Images from \myurl{https://docs.docker.com/get-started/\#containers-and-virtual-machines}{Docker documentation}}
\end{frame}

\begin{frame}{Build a Container}
  \begin{itemize}
    \item Administrative access is required to build!
    \item Two examples:
    \begin{itemize}
      \item Docker, you can't run these directly on the cluster without:
      \item Singularity, \code{module spider singularity}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Opinions}
  \begin{itemize}
    \item Take notes! \myurl{https://boostnote.io/}{BoostNote}
    \item Get serious about learning \code{vim} or \code{emacs}
    \item Master one language, learn others
    \item Learn common idioms in your mastery language
    \item We discussed imperative languages today
    \begin{itemize}
      \item Have you ever tried a functional language?
      \item Do you know the difference?
    \end{itemize}
    \item Know a little MPI, have you tried PGAS?
    \item Follow developers on social media
    \item Try reading other people's code on GitHub
    \item Ask and answer questions on Stack Overflow
    \item Submit pull requests on GitHub
  \end{itemize}
\end{frame}

\begin{frame}{Questions}
    \centering
    \Huge Questions? \\
    \Huge \myurl{https://goo.gl/forms/EFay5sPg8odGqtVG2}{Quick Survey}
\end{frame}

\end{document}
