git clone https://bitbucket.org/barrymoo/crc-dev-tools
cd crc-dev-tools/code
git submodule update --init

# If you want to compile the latex
# -> install git-lfs from package manager
git lfs install
git lfs pull
